# Unicorn Annotations

This is the repository that will demonstrate and implement the uCatascopia Monitoring annotations



## Contents

This repo at it current version consists of two modules

1. ***Unicorn-Catascopia-Annotations*** that implements the annotation and annotation logic.
2. ***very-dummy-app*** a hello world like __Spring Boot Application__

## Unicorn-Catascopia-Annotations description

### Prerequisites
* ``Java 8``
* ``Maven 3.x``
* ``AspectJ 1.9.1``

#### External Dependencies

* uCatascopia-Agent project

    1. ```git clone https://gitlab.com/unicorn-project/uCatascopia-Agent.git```
    2. cd to the uCatascopia-Agent
    3. run ```mvn clean install```

* Catascopia-Probe-Repo

    1. ```git clone https://gitlab.com/unicorn-project/Catascopia-Probe-Repo.git```
    2. cd to the Catascopia-Probe-Repo
    3. run ```mvn clean install```

### Project Setup
```pom

        <!-- pom.xml file -->

        ...

        <!--AspectJ aspectjrt version 1.9.1 dependency -->
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjrt</artifactId>
            <version>1.9.1</version>
        </dependency>
        
        <!-- Maven plugin in order to build properly the aspect -->
        <build>
               <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>aspectj-maven-plugin</artifactId>
                        <version>1.11</version>
                        <configuration>
                            <complianceLevel>1.8</complianceLevel>
                            <source>1.8</source>
                            <target>1.8</target>
        
                        </configuration>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>compile</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
        
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-compiler-plugin</artifactId>
                        <version>3.7.0</version>
                        <configuration>
                            <source>1.8</source>
                            <target>1.8</target>
                        </configuration>
                    </plugin>
        
                </plugins>
        </build>
        
        ...
           
```

## very-dummy-app Demo annotation

### Prerequisites
* ``Java 8``
* ``Maven 3.x``
* ``Spring-Boot ``
* ``catascopia.properties`` file should be in the classpath


### Project Setup

```

....(more pom code)...

        <dependency>
            <groupId>eu.unicornH2020.annotations</groupId>
            <artifactId>Unicorn-Catascopia-Annotations</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

....(more pom code)...   


    <build>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>aspectj-maven-plugin</artifactId>
                <version>1.11</version>
                <configuration>
                    <complianceLevel>1.8</complianceLevel>
                    <source>1.8</source>
                    <target>1.8</target>
                    <aspectLibraries>
                        <aspectLibrary>
                            <groupId>eu.unicornH2020.annotations</groupId>
                            <artifactId>Unicorn-Catascopia-Annotations</artifactId>
                        </aspectLibrary>
                    </aspectLibraries>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

....(more pom code)...   
           

        </plugins>

    </build>

```

### Usage

```aidl
package eu.unicornH2020.dummy;

import eu.unicornH2020.annotations.UnicornCatascopiaMonitoring;
import eu.unicornH2020.catascopia.probe.CatascopiaMetricFilter;
import eu.unicornH2020.catascopia.probe.CatascopiaMetricProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@Import({CatascopiaMetricFilter.class, CatascopiaMetricProvider.class})
@UnicornCatascopiaMonitoring
public class DummyApp {

    public static void main(String[] args) {
        SpringApplication.run(DummyApp.class, args);

    }
}

```