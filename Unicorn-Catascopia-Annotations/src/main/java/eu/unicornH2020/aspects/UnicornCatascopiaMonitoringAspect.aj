package eu.unicornH2020.aspects;

import eu.unicornH2020.catascopia.agent.Catascopia;
import eu.unicornH2020.catascopia.agent.exceptions.*;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Aspect
public class UnicornCatascopiaMonitoringAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnicornCatascopiaMonitoringAspect.class);


    @Pointcut("@annotation(eu.unicornH2020.annotations.UnicornCatascopiaMonitoring)")
    public void unicornCatascopiaMonitorAnnotation(){}

/*
    @Pointcut("execution(public * *(..))")
    public void atExecution(){}
*/

    @After("unicornCatascopiaMonitorAnnotation()")
    public void unicornCatascopiaMonitorMethod(JoinPoint pjp){

        LOGGER.info("Starting Catascopia Agent ...");
        try {
            Catascopia agent = new Catascopia();
            agent.startMonitoring();
        } catch (CatascopiaParseConfigException e) {
            e.printStackTrace();
        } catch (CatascopiaProbeNotFoundException e) {
            e.printStackTrace();
        } catch (CatascopiaProbeInstantiationException e) {
            e.printStackTrace();
        } catch (CatascopiaServiceConnectorNotFoundException e) {
            e.printStackTrace();
        } catch (CatascopiaServiceConnectorInstantiationException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        LOGGER.info("Catascopia Agent Started!");

    }

}
