package eu.unicornH2020.dummy;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DummyController {

    @RequestMapping(method = RequestMethod.GET, path = "/index")
    public String index(){
        return "Hello World";
    }
}
