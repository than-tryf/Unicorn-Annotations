package eu.unicornH2020.dummy;

import eu.unicornH2020.annotations.UnicornCatascopiaMonitoring;
import eu.unicornH2020.catascopia.probe.CatascopiaMetricFilter;
import eu.unicornH2020.catascopia.probe.CatascopiaMetricProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@Import({CatascopiaMetricFilter.class, CatascopiaMetricProvider.class})
@UnicornCatascopiaMonitoring
public class DummyApp {

    public static void main(String[] args) {
        SpringApplication.run(DummyApp.class, args);

    }
}
